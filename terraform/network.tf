# Network definitions

# Identify AZs to use
data "aws_availability_zones" "challenge_available_azs" {
  state = "available"
}

# VPC
resource "aws_vpc" "challenge_vpc" {
  # checkov:skip=CKV2_AWS_11:No need for Flow Log in this challenge
  cidr_block           = var.vpc_cidr
  instance_tenancy     = "default"
  enable_dns_support   = true
  enable_dns_hostnames = true
  tags = {
    Name = "challenge_vpc"
  }
}

# Default Route Table
resource "aws_default_route_table" "challenge_default_route_table" {
  default_route_table_id = aws_vpc.challenge_vpc.default_route_table_id
  tags = {
    Name = "challenge_default_route_table"
  }
}

# Default Security Group
resource "aws_default_security_group" "challenge_default_security_group" {
  vpc_id = aws_vpc.challenge_vpc.id
  // All Ingress and Egress blocked by Default
  //  ingress {
  //  }
  //  egress {
  //  }
  tags = {
    Name = "challenge_default_security_group"
  }
}

# Default NACL
resource "aws_default_network_acl" "default" {
  default_network_acl_id = aws_vpc.challenge_vpc.default_network_acl_id
  // All Ingress and Egress blocked by Default
  //  ingress {
  //  }
  //  egress {
  //  }
  tags = {
    Name = "challenge_default_nacl"
  }
}

# Internet Gateway
resource "aws_internet_gateway" "challenge_igw" {
  vpc_id = aws_vpc.challenge_vpc.id
  tags = {
    Name = "challenge_igw"
  }
}

# Public Subnets
resource "aws_subnet" "challenge_public_subnet" {
  count                   = var.azs_to_use
  cidr_block              = cidrsubnet(var.vpc_cidr, 8, count.index)
  vpc_id                  = aws_vpc.challenge_vpc.id
  availability_zone       = data.aws_availability_zones.challenge_available_azs.names[count.index]
  map_public_ip_on_launch = false
  tags = {
    Name = "challenge_public_${count.index}"
  }
}

# Public Route Table
resource "aws_route_table" "challenge_public_route_table" {
  vpc_id = aws_vpc.challenge_vpc.id
  route {
    # Associated subnet can reach public internet
    cidr_block = "0.0.0.0/0"
    # Which internet gateway to use
    gateway_id = aws_internet_gateway.challenge_igw.id
  }
  tags = {
    Name = "challenge_public_route_table"
  }
}

resource "aws_route_table_association" "challenge_public_rt_assoc" {
  count          = var.azs_to_use
  route_table_id = aws_route_table.challenge_public_route_table.id
  subnet_id      = aws_subnet.challenge_public_subnet.*.id[count.index]
}

# Public NACL
resource "aws_network_acl" "challenge_public_nacl" {
  vpc_id     = aws_vpc.challenge_vpc.id
  subnet_ids = aws_subnet.challenge_public_subnet.*.id
  # Allow all ingress
  ingress {
    rule_no    = 100
    action     = "allow"
    protocol   = "-1"
    from_port  = 0
    to_port    = 0
    cidr_block = "0.0.0.0/0"
  }
  # Allow all egress
  egress {
    rule_no    = 100
    action     = "allow"
    protocol   = -1
    from_port  = 0
    to_port    = 0
    cidr_block = "0.0.0.0/0"
  }
  tags = {
    "Name" = "challenge_public_nacl"
  }
}

# Private Subnets
resource "aws_subnet" "challenge_private_subnet" {
  count                   = var.azs_to_use
  cidr_block              = cidrsubnet(var.vpc_cidr, 8, count.index + var.azs_to_use)
  vpc_id                  = aws_vpc.challenge_vpc.id
  availability_zone       = data.aws_availability_zones.challenge_available_azs.names[count.index]
  map_public_ip_on_launch = false
  tags = {
    Name = "challenge_private_${count.index}"
  }
}

# Elastic IP for the NAT Gateway
resource "aws_eip" "challenge_natgw_eip" {
  vpc        = true
  depends_on = [aws_internet_gateway.challenge_igw]
  tags = {
    Name = "challenge_natgw_eip"
  }
}

# NAT Gateway
resource "aws_nat_gateway" "challenge_natgw" {
  connectivity_type = "public"
  subnet_id         = aws_subnet.challenge_public_subnet.*.id[0]
  allocation_id     = aws_eip.challenge_natgw_eip.id
  tags = {
    Name = "challenge_natgw"
  }
  # To ensure proper ordering, it is recommended to add an explicit dependency on the Internet Gateway for the VPC.
  depends_on = [aws_internet_gateway.challenge_igw]
}

# Private Route Table
resource "aws_route_table" "challenge_private_route_table" {
  vpc_id = aws_vpc.challenge_vpc.id
  route {
    # Associated subnet can reach public internet
    cidr_block = "0.0.0.0/0"
    # Which internet gateway to use
    nat_gateway_id = aws_nat_gateway.challenge_natgw.id
  }
  tags = {
    Name = "challenge_private_route_table"
  }
}

resource "aws_route_table_association" "challenge_private_rt_assoc" {
  count          = var.azs_to_use
  route_table_id = aws_route_table.challenge_private_route_table.id
  subnet_id      = aws_subnet.challenge_private_subnet.*.id[count.index]
}

# Private NACL
resource "aws_network_acl" "challenge_private_nacl" {
  vpc_id     = aws_vpc.challenge_vpc.id
  subnet_ids = aws_subnet.challenge_private_subnet.*.id
  # Port 5432 for PostgreSQL
  ingress {
    rule_no    = 100
    action     = "allow"
    protocol   = "tcp"
    from_port  = 5432
    to_port    = 5432
    cidr_block = var.vpc_cidr
  }
  # Port 6379 for Redis
  ingress {
    rule_no    = 200
    action     = "allow"
    protocol   = "tcp"
    from_port  = 6379
    to_port    = 6379
    cidr_block = var.vpc_cidr
  }
  # Allow all egress
  egress {
    rule_no    = 100
    action     = "allow"
    protocol   = -1
    from_port  = 0
    to_port    = 0
    cidr_block = "0.0.0.0/0"
  }
  tags = {
    "Name" = "challenge_private_nacl"
  }
}
