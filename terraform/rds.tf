# RDS declaration

# Subnet Group
resource "aws_db_subnet_group" "challenge_rds_subnet_group" {
  name        = "challenge"
  subnet_ids  = aws_subnet.challenge_private_subnet.*.id
  description = "For the challenge PostgreSQL RDS"
  tags = {
    Name = "Challenge RDS Subnet Group"
  }
}

# RDS Instance
resource "aws_db_instance" "postgresql_rds" {
  # checkov:skip=CKV_AWS_16:No need for Encryption at Rest in this challenge
  # checkov:skip=CKV_AWS_129:No need to enable logs in this challenge
  # checkov:skip=CKV_AWS_118:No need for Enhanced Monitoring in this challenge
  # checkov:skip=CKV_AWS_157:No need for Multi-AZ in this challenge
  # checkov:skip=CKV_AWS_161:No need for IAM authentication in this challenge
  # checkov:skip=CKV2_AWS_27:No need for Query Logging in this challenge
  identifier             = "challenge" # RDS name
  name                   = "challenge" # Database name
  engine                 = "postgres"
  engine_version         = "12.7"
  availability_zone      = data.aws_availability_zones.challenge_available_azs.names[0]
  db_subnet_group_name   = aws_db_subnet_group.challenge_rds_subnet_group.name
  instance_class         = "db.t2.micro"
  allocated_storage      = 20
  max_allocated_storage  = 0
  vpc_security_group_ids = [aws_security_group.challenge_postgresql_sg.id]
  parameter_group_name   = "default.postgres12"
  skip_final_snapshot    = true
  username               = var.rds_root_user
  password               = var.rds_root_user_password
  tags = {
    Name = "challenge"
  }
}
