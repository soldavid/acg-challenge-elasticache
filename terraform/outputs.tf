# Output values

output "rds_endpoint" {
  value = aws_db_instance.postgresql_rds.endpoint
}

output "bastion_ip" {
  value = aws_instance.bastion.public_ip
}

output "redis_endpoint" {
  value = "${aws_elasticache_cluster.redis_instance.cache_nodes[0].address}:${aws_elasticache_cluster.redis_instance.cache_nodes[0].port}"
}
