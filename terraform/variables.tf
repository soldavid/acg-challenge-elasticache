# Variables used by the templates

variable "region" {
  description = "AWS Region"
  type        = string
  default     = "us-west-2"
}

variable "azs_to_use" {
  description = "Number of AZs to use"
  type        = number
  default     = 2
}

variable "profile" {
  description = "AWS Profile"
  type        = string
  default     = "david21"
}

variable "challenge_tag" {
  description = "Tag Value to use for all the resources"
  type        = string
  default     = "elasticache"
}

variable "challenge_created_at" {
  description = "Creation Date"
  type        = string
  default     = "2021-06-16 22:00"
  // formatdate("YYYY-MM-DD HH:mm", timestamp())
}

variable "vpc_cidr" {
  description = "VPC CIDR"
  type        = string
  default     = "192.168.0.0/16"
}

variable "rds_root_user" {
  description = "RDS root user"
  type        = string
  default     = "postgres"
}

variable "rds_root_user_password" {
  description = "RDS root user password"
  type        = string
  sensitive   = true
}

# Get my IP address for the Bastion SG
data "http" "my_ip_address" {
  url = "http://ipv4.icanhazip.com"
}
