# Requires only the AWS provider
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

# AWS Provider configuration, using the variables from variables.tf
provider "aws" {
  profile = var.profile
  region  = var.region
  default_tags {
    tags = {
      "CloudGuruChallenge" = var.challenge_tag,
      "CreatedAt"          = var.challenge_created_at
    }
  }
}
