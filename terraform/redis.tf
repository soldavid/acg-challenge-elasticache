# Redis Database

# Subnet Group
resource "aws_elasticache_subnet_group" "redis_db" {
  name        = "challenge"
  description = "Redis cache for the challenge"
  subnet_ids  = [aws_subnet.challenge_private_subnet[0].id]
  tags = {
    Name = "challenge"
  }
}

resource "aws_elasticache_cluster" "redis_instance" {
  # checkov:skip=CKV_AWS_134:No need to enable automatic backups in this challenge
  cluster_id = "challenge"
  # description          = "Redis DB for the challenge"
  engine               = "redis"
  node_type            = "cache.t3.micro"
  num_cache_nodes      = 1
  parameter_group_name = "default.redis6.x"
  engine_version       = "6.x"
  port                 = 6379
  availability_zone    = data.aws_availability_zones.challenge_available_azs.names[0]
  security_group_ids   = [aws_security_group.challenge_redis_sg.id]
  subnet_group_name    = aws_elasticache_subnet_group.redis_db.name
  tags = {
    Name = "challenge"
  }
}
