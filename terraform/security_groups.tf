resource "aws_security_group" "challenge_bastion_sg" {
  name        = "challenge_bastion_sg"
  description = "Allow connection to bastion"
  vpc_id      = aws_vpc.challenge_vpc.id
  ingress {
    description = "SSH from My IP"
    protocol    = "tcp"
    from_port   = 22
    to_port     = 22
    cidr_blocks = ["${chomp(data.http.my_ip_address.body)}/32"]
  }
  ingress {
    description = "HTTP from My IP"
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["${chomp(data.http.my_ip_address.body)}/32"]
  }
  ingress {
    description = "App from My IP"
    protocol    = "tcp"
    from_port   = 5000
    to_port     = 5000
    cidr_blocks = ["${chomp(data.http.my_ip_address.body)}/32"]
  }
  egress {
    description = "Allow All"
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "challenge_bastion_sg"
  }
}

resource "aws_security_group" "challenge_postgresql_sg" {
  name        = "challenge_postgresql_sg"
  description = "Allow connection to PostgreSQL from Bastion"
  vpc_id      = aws_vpc.challenge_vpc.id
  ingress {
    description     = "PostgreSQL from VPC"
    protocol        = "tcp"
    from_port       = 5432
    to_port         = 5432
    security_groups = [aws_security_group.challenge_bastion_sg.id]
  }
  // Deny all
  // egress {
  // }
  tags = {
    Name = "challenge_postgresql_sg"
  }
}

resource "aws_security_group" "challenge_redis_sg" {
  name        = "challenge_redis_sg"
  description = "Allow connection to Redis from Bastion"
  vpc_id      = aws_vpc.challenge_vpc.id
  ingress {
    description     = "Redis from VPC"
    protocol        = "tcp"
    from_port       = 6379
    to_port         = 6379
    security_groups = [aws_security_group.challenge_bastion_sg.id]
  }
  // Deny all
  // egress {
  // }
  tags = {
    Name = "challenge_redis_sg"
  }
}
