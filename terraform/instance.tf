# Web Server Instance

# Finds the latest Linux 2 AMI for ARM
data "aws_ami" "amazon_linux_2" {
  most_recent = true
  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-2.*-arm64-gp2"]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }
  filter {
    name   = "architecture"
    values = ["arm64"]
  }
  owners = ["137112412989"] # Amazon
}

# Uploading existing public key
resource "aws_key_pair" "challenge" {
  key_name   = "challenge-key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDUve+qrkaRkWM5Mw91Whn7Rwne+WVU78XcAMwyIHwC+IMESIp4oo1U7jM+Yjsbv+5qvJ/LDeQEgYA2XKQPN8JYKqENI7vWJWcq+3aTPHQYKtR2BkX4daedtgyRIWipUa5F4peTzEktubIZLCiiGqxJlULdGzkoJjutdf+CxtvmtQSQ2BBMx9Dg2dx465SMiBuXLgkYS+WqXQ7lTFMq70F9VffK8z8oLEfVtnx2nL4v8fA43vBG3ryqMyimQh3Vkb8zk50TTowXFI0Xj+E8EIiyoXcS5Wr6gwNepsbj2yH8nhwKMi3KcbhjH2TKWCp/2flZlElpHBlJXWYiaz4WKKrG2LkvKG/muQTLPL03cTzdcRC2Dbbkgr4F+ASPI1vn3QOdg2AwVG1aYqJkchsvak14HlkMvBXSxorl1fIqyi4/xX8TfIBDQnsboAfGAfU+5UG9eM0Ln3/YOECCMw4m61J4dwJTyWx2yNdW84SZIJPiRc7fJ3Hnt1hoItXfZVGP0gs= david@corona"
}

# Prepare Cloud Init script
data "cloudinit_config" "user_data" {
  gzip          = false
  base64_encode = false
  part {
    content_type = "text/cloud-config"
    content      = templatefile("user_data.yaml", {})
  }
}

resource "aws_instance" "bastion" {
  # checkov:skip=CKV_AWS_8:No need for EBS Encryption in this challenge
  # checkov:skip=CKV_AWS_79:No need to change Instance Metadata Service version in this challenge
  # checkov:skip=CKV_AWS_88:The instance will server as Web and Bastion server, Public IP is needed
  # checkov:skip=CKV_AWS_135:No need for an EBS Optimized instance in this challenge
  # checkov:skip=CKV_AWS_126:No need for Detailed Monitoring in this challenge
  ami                         = data.aws_ami.amazon_linux_2.id
  instance_type               = "t4g.micro"
  subnet_id                   = aws_subnet.challenge_public_subnet[0].id
  associate_public_ip_address = true
  credit_specification {
    cpu_credits = "standard"
  }
  root_block_device {
    volume_type = "gp3"
    volume_size = 20
  }
  vpc_security_group_ids = [aws_security_group.challenge_bastion_sg.id]
  key_name               = aws_key_pair.challenge.key_name
  user_data              = data.cloudinit_config.user_data.rendered
  tags = {
    Name = "challenge"
  }
}
