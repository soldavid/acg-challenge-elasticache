# Instructions

Extra instructions for creating the infrastructure

## Create Key Pair

We need to create a keypair that will be assigned to the bastion instance

```bash
ssh-keygen -f challenge_key
```

```text
Generating public/private rsa key pair.
Enter passphrase (empty for no passphrase): 
Enter same passphrase again: 
Your identification has been saved in challenge_key
Your public key has been saved in challenge_key.pub
The key fingerprint is:
SHA256:L8S9nWke/YDjimWLgmJjf1Jro7dlwf1PT1iqyByA9yo david@corona
The key's randomart image is:
+---[RSA 3072]----+
|                 |
|                 |
|                 |
|       + o       |
|      . S o     .|
|      .o = + = + |
|     o .+ B X * .|
|  = o Eo X B * = |
| o +o*.=+ *.+ . o|
+----[SHA256]-----+
```

## Set password variable

To prevent Terraform to ask for it every time

```bash
export TF_VAR_rds_root_user_password=<password> 
```

## Validate Terraform templates

```bash
terraform fmt
terraform validate
checkov -d . --quiet
```

## Server config

```bash
sudo yum install postgresql
sudo yum install python-pip
sudo python -m pip install -U pip
sudo yum install python-psycopg2
python -m pip install flask configparser redis
wget https://github.com/ACloudGuru/elastic-cache-challenge/archive/refs/heads/master.zip
unzip master.zip
psql -h challenge.c7vkjayit0iz.us-west-2.rds.amazonaws.com -U postgres -f install.sql challenge
sudo amazon-linux-extras install nginx1
sudo cp ./config/nginx-app.conf /etc/nginx/default.d/
sudo service nginx start
python app.py
```

## Test service without optimization

```bash
for i in {1..10}; do curl -s http://34.222.229.118/app | grep Elapsed | cut -d ' ' -f 3; done
```

Result:

```text
5.02370s
5.03357s
5.04021s
5.03253s
5.03906s
5.03581s
5.03973s
5.03937s
5.03995s
5.09734s
```

## Test service with cache optimization

```bash
for i in {1..10}; do curl -s http://34.222.229.118/app | grep Elapsed | cut -d ' ' -f 3; done
```

Result:

```text
5.03411s
0.00277s
0.00288s
0.00271s
0.00294s
0.00278s
0.00264s
0.00302s
0.00285s
0.00308s
```

## Log from the application

```text
 * Serving Flask app "app" (lazy loading)
 * Environment: production
   WARNING: This is a development server. Do not use it in a production deployment.
   Use a production WSGI server instead.
 * Debug mode: off
 * Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)
Connecting to the Redis database challenge.yigehd.0001.usw2.cache.amazonaws.com:6379...
Redis connection open
Connecting to the PostgreSQL database...
('Found value in PostgreSQL: ', ('PostgreSQL 12.7 on x86_64-pc-linux-gnu, compiled by gcc (GCC) 4.8.3 20140911 (Red Hat 4.8.3-9), 64-bit',))
Value stored in Redis
PostgreSQL connection is now closed
Redis connection closed
127.0.0.1 - - [31/Jul/2021 02:26:54] "GET / HTTP/1.0" 200 -
Connecting to the Redis database challenge.yigehd.0001.usw2.cache.amazonaws.com:6379...
Redis connection open
Cached value found: PostgreSQL 12.7 on x86_64-pc-linux-gnu, compiled by gcc (GCC) 4.8.3 20140911 (Red Hat 4.8.3-9), 64-bit
Redis connection closed
127.0.0.1 - - [31/Jul/2021 02:26:54] "GET / HTTP/1.0" 200 -
Connecting to the Redis database challenge.yigehd.0001.usw2.cache.amazonaws.com:6379...
Redis connection open
Cached value found: PostgreSQL 12.7 on x86_64-pc-linux-gnu, compiled by gcc (GCC) 4.8.3 20140911 (Red Hat 4.8.3-9), 64-bit
Redis connection closed
127.0.0.1 - - [31/Jul/2021 02:26:55] "GET / HTTP/1.0" 200 -
Connecting to the Redis database challenge.yigehd.0001.usw2.cache.amazonaws.com:6379...
Redis connection open
Cached value found: PostgreSQL 12.7 on x86_64-pc-linux-gnu, compiled by gcc (GCC) 4.8.3 20140911 (Red Hat 4.8.3-9), 64-bit
Redis connection closed
127.0.0.1 - - [31/Jul/2021 02:26:55] "GET / HTTP/1.0" 200 -
Connecting to the Redis database challenge.yigehd.0001.usw2.cache.amazonaws.com:6379...
Redis connection open
Cached value found: PostgreSQL 12.7 on x86_64-pc-linux-gnu, compiled by gcc (GCC) 4.8.3 20140911 (Red Hat 4.8.3-9), 64-bit
Redis connection closed
127.0.0.1 - - [31/Jul/2021 02:26:55] "GET / HTTP/1.0" 200 -
Connecting to the Redis database challenge.yigehd.0001.usw2.cache.amazonaws.com:6379...
Redis connection open
Cached value found: PostgreSQL 12.7 on x86_64-pc-linux-gnu, compiled by gcc (GCC) 4.8.3 20140911 (Red Hat 4.8.3-9), 64-bit
Redis connection closed
127.0.0.1 - - [31/Jul/2021 02:26:55] "GET / HTTP/1.0" 200 -
Connecting to the Redis database challenge.yigehd.0001.usw2.cache.amazonaws.com:6379...
Redis connection open
Cached value found: PostgreSQL 12.7 on x86_64-pc-linux-gnu, compiled by gcc (GCC) 4.8.3 20140911 (Red Hat 4.8.3-9), 64-bit
Redis connection closed
127.0.0.1 - - [31/Jul/2021 02:26:55] "GET / HTTP/1.0" 200 -
Connecting to the Redis database challenge.yigehd.0001.usw2.cache.amazonaws.com:6379...
Redis connection open
Cached value found: PostgreSQL 12.7 on x86_64-pc-linux-gnu, compiled by gcc (GCC) 4.8.3 20140911 (Red Hat 4.8.3-9), 64-bit
Redis connection closed
127.0.0.1 - - [31/Jul/2021 02:26:55] "GET / HTTP/1.0" 200 -
Connecting to the Redis database challenge.yigehd.0001.usw2.cache.amazonaws.com:6379...
Redis connection open
Cached value found: PostgreSQL 12.7 on x86_64-pc-linux-gnu, compiled by gcc (GCC) 4.8.3 20140911 (Red Hat 4.8.3-9), 64-bit
Redis connection closed
127.0.0.1 - - [31/Jul/2021 02:26:55] "GET / HTTP/1.0" 200 -
Connecting to the Redis database challenge.yigehd.0001.usw2.cache.amazonaws.com:6379...
Redis connection open
Cached value found: PostgreSQL 12.7 on x86_64-pc-linux-gnu, compiled by gcc (GCC) 4.8.3 20140911 (Red Hat 4.8.3-9), 64-bit
Redis connection closed
127.0.0.1 - - [31/Jul/2021 02:26:56] "GET / HTTP/1.0" 200 -
```

## Destroy the infrastructure

```bash
terraform destroy
```
