# /usr/bin/python2.7
import time
from configparser import ConfigParser

import psycopg2
from flask import Flask, render_template, g, abort

# Adding redis
import redis


def config(filename='config/database.ini', section='postgresql'):
    # create a parser
    parser = ConfigParser()
    # read config file
    parser.read(filename)

    # get section, default to postgresql
    db = {}
    if parser.has_section(section):
        params = parser.items(section)
        for param in params:
            db[param[0]] = param[1]
    else:
        raise Exception('Section {0} not found in the {1} file'.format(section, filename))

    return db


def fetch(sql):

    # Key value
    redis_key = 'cached_value'

    # Connect to Redis
    try:
        # read connection parameters
        redis_params = config(section="redis")
        # connect to the redis server
        print('Connecting to the Redis database ' + redis_params["host"] + "...")
        redis_conn = redis.Redis.from_url("redis://" + redis_params["host"])
        print('Redis connection open')
    except Exception as error:
        print("Error:", error)
        abort(600)

    # Try to get the value from the cache
    try:
        cached_value = redis_conn.get(redis_key)
        if cached_value is not None:
            value = cached_value.decode('utf-8')
            print("Cached value found: " + value)
            redis_conn.close()
            print('Redis connection closed')
            return value
    except Exception as error:
        print('fetch error:', error)
        raise error

    # Value was not found in the cache, proceed to find it in PostgreSQL

    # connect to database listed in database.ini
    conn = connect()
    if conn is not None:
        cur = conn.cursor()
        cur.execute(sql)

        # fetch one row
        retval = cur.fetchone()
        print("Found value in PostgreSQL: " + retval)

        # Cache the value in Redis
        redis_conn.set(redis_key, retval.encode('utf-8'), ex=60)
        print("Value stored in Redis")

        # close db connection
        cur.close()
        conn.close()
        print("PostgreSQL connection is now closed")
        redis_conn.close()
        print('Redis connection closed')

        return retval
    else:
        return None


def connect():
    """ Connect to the PostgreSQL database server and return a cursor """
    conn = None
    try:
        # read connection parameters
        params = config()

        # connect to the PostgreSQL server
        print('Connecting to the PostgreSQL database...')
        conn = psycopg2.connect(**params)

    except (Exception, psycopg2.DatabaseError) as error:
        print("Error:", error)
        conn = None

    else:
        # return a conn
        return conn


app = Flask(__name__)


@app.before_request
def before_request():
    g.request_start_time = time.time()
    g.request_time = lambda: "%.5fs" % (time.time() - g.request_start_time)


@app.route("/")
def index():
    sql = 'SELECT slow_version();'
    db_result = fetch(sql)

    if (db_result):
        db_version = ''.join(db_result)
    else:
        abort(500)
    params = config()
    return render_template('index.html', db_version=db_version, db_host=params['host'])


if __name__ == "__main__":  # on running python app.py
    app.run()  # run the flask app
